<!DOCTYPE html>
<!--[if !IE]><!-->
<html lang="$ContentLocale">
<!--<![endif]-->
<!--[if IE 6 ]><html lang="$ContentLocale" class="ie ie6"><![endif]-->
<!--[if IE 7 ]><html lang="$ContentLocale" class="ie ie7"><![endif]-->
<!--[if IE 8 ]><html lang="$ContentLocale" class="ie ie8"><![endif]-->
<head>
	<% base_tag %>
	<title><% if $MetaTitle %>$MetaTitle<% else %>$Title<% end_if %> &raquo; $SiteConfig.Title</title>
	<meta charset="utf-8">
	<meta property="og:image" content="$BaseHref$LinkPhoto" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic,300,500,500italic,700,700italic,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	$MetaTags(false)
	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="apple-touch-icon" sizes="57x57" href="$ThemeDir/images/favicon/apple-icon-57x57.png?v=3">
	<link rel="apple-touch-icon" sizes="60x60" href="$ThemeDir/images/favicon/apple-icon-60x60.png?v=3">
	<link rel="apple-touch-icon" sizes="72x72" href="$ThemeDir/images/favicon/apple-icon-72x72.png?v=3">
	<link rel="apple-touch-icon" sizes="76x76" href="$ThemeDir/images/favicon/apple-icon-76x76.png?v=3">
	<link rel="apple-touch-icon" sizes="114x114" href="$ThemeDir/images/favicon/apple-icon-114x114.png?v=3">
	<link rel="apple-touch-icon" sizes="120x120" href="$ThemeDir/images/favicon/apple-icon-120x120.png?v=3">
	<link rel="apple-touch-icon" sizes="144x144" href="$ThemeDir/images/favicon/apple-icon-144x144.png?v=3">
	<link rel="apple-touch-icon" sizes="152x152" href="$ThemeDir/images/favicon/apple-icon-152x152.png?v=3">
	<link rel="apple-touch-icon" sizes="180x180" href="$ThemeDir/images/favicon/apple-icon-180x180.png?v=3">
	<link rel="icon" type="image/png" sizes="192x192"  href="$ThemeDir/images/favicon/android-icon-192x192.png?v=3">
	<link rel="icon" type="image/png" sizes="32x32" href="$ThemeDir/images/favicon/favicon-32x32.png?v=3">
	<link rel="icon" type="image/png" sizes="96x96" href="$ThemeDir/images/favicon/favicon-96x96.png?v=3">
	<link rel="icon" type="image/png" sizes="16x16" href="$ThemeDir/images/favicon/favicon-16x16.png?v=3">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="$ThemeDir/images/favicon/ms-icon-144x144.png?v=3">
	<meta name="theme-color" content="#ffffff">	
</head>

<body>
<% include Header %>
$Layout
<% include Footer %>

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>
    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">
        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <!--  Controls are self-explanatory. Order can be changed. -->
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>
</body>
</html>
