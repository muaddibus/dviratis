$(document).ready(function () {
	initPhotoSwipeFromDOM('.galerija');
	if ($("#MapID_Main")) {
	    var mapob = document.getElementById("MapID_Main");
	    var lat=$("#MapID_Main").attr("data-lat");
	    var lng=$("#MapID_Main").attr("data-lng");
	    var type=$("#MapID_Main").attr("data-type");
	    var zoomas=parseInt($("#MapID_Main").attr("data-zoom"));
	    var vieta=new google.maps.LatLng(lat,lng);
	    map=new google.maps.Map(mapob,{center:vieta,disableDefaultUI: true,draggable: true, scrollwheel: true, panControl: true, zoom: zoomas, mapTypeId:google.maps.MapTypeId.ROADMAP});
	    // foreach is ajax
	    $.getJSON( "takai/json", function( data ) {
		var markers = [];
		var mcOptions = {gridSize: 50, maxZoom: 15};
		data.items.forEach(function(entry) {
			var vieta = new google.maps.LatLng(entry.Latitude,entry.Longitude);
			markers.push(new google.maps.Marker({position: vieta,map: map,title:entry.Title}));
		});
		
		var mc = new MarkerClusterer(map, markers, mcOptions);

	    });
	    var marker = new google.maps.Marker({position: vieta,map: map});

	} else {
//	    if ($(".googleMap")) {
//		allMaps();
//	    }
	}
});

function initMap(id) {
    var mapob = document.getElementById(id);
    var lat=$("#"+id).attr("data-lat");
    var lng=$("#"+id).attr("data-lng");
    var type=$("#"+id).attr("data-type");
    var zoomas=parseInt($("#"+id).attr("data-zoom"));
    var vieta=new google.maps.LatLng(lat,lng);
    if (type=="withcontrols") {
	var map=new google.maps.Map(mapob,{center:vieta,disableDefaultUI: true,draggable: true, scrollwheel: true, panControl: true, zoom: zoomas, mapTypeId:google.maps.MapTypeId.SATELLITE});
	} else {
	var map=new google.maps.Map(mapob,{center:vieta,disableDefaultUI: true,draggable: false, scrollwheel: false, panControl: false, zoom: zoomas, mapTypeId:google.maps.MapTypeId.ROADMAP});
    }
    var marker = new google.maps.Marker({
        position: vieta,
            map: map
        });
}

function allMaps() {
	$(".googleMap").each(function () {
		initMap($(this).attr("id"));
	});
}
