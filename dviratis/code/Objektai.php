<?php
class Objektai extends Page {

        private static $db = array(
        );
        private static $has_many = array(
                'Objektai' => 'Objektas',
        );
        private static $has_one = array(
        );

        function getCMSFields() {
                $fields = parent::getCMSFields();

                $gridFieldConfig = GridFieldConfig::create()->addComponents(
                        new GridFieldToolbarHeader(),
                        new GridFieldAddNewButton('toolbar-header-right'),
                        new GridFieldSortableHeader(),
                        new GridFieldDataColumns(),
                        new GridFieldPaginator(10),
                        new GridFieldEditButton(),
                        new GridFieldDeleteAction(),
                        new GridFieldDetailForm()
                );
                $gridField = new GridField("Objektai", "Objektai", $this->Objektai(), $gridFieldConfig);
                $fields->addFieldToTab("Root.Objektai", $gridField);

                return $fields;
        }

}
class Objektai_Controller extends Page_Controller {
    //Allow our 'show' function as a URL action
    static $allowed_actions = array(
        'show',
        'NaujasObjektas',
        'json',
    );
    public function json(SS_HTTPRequest $request) {
	$f = new JSONDataFormatter();
        $this->response->addHeader('Content-Type', 'application/json');
        return $f->convertDataObjectSet(Objektas::get("Objektas")->filter(array("Patvirtinta"=>true)));
    }

    public function NaujasObjektas(){
	$textarea = new HtmlEditorField('Description', 'Aprasymas');
	$textarea->setRows(3)->addExtraClass("googlemap");
    	$fields = new FieldList(
    		new TextField('Title', 'Pavadinimas'),
    		$textarea,
    		new HiddenField('ObjektaiID','ObjektaiID',$this->ID),
    		new GoogleMapField($this,'Vieta zemelapyje')
    	);
    	$validator = new RequiredFields('Title', 'Description');
    	$actions = new FieldList(    
    	        new FormAction('Ikelti', 'Ikelti')
    	);
    	$form = new Form($this, 'NaujasObjektas', $fields, $actions, $validator);           
    	return $form;
    }
    function Ikelti($data, $form) {
        $obj = new Objektas();
        $form->saveInto($obj);
        $obj->write();
	$this->redirect( Director::baseURL() . "/?success=1" );
    }
    //Get the current staffMember from the URL, if any
    public function getObjektas()
    {
        $Params = $this->getURLParams();
         
        if(is_numeric($Params['ID']) && $Objektas = DataObject::get_by_id('Objektas', (int)$Params['ID']))
        {       
            return $Objektas;
        }
    }
     
    //Displays the StaffMember detail page, using StaffPage_show.ss template
    public function show() 
    {       
        if($Objektas = $this->getObjektas())
        {
            $Data = array(
                'Objektas' => $Objektas
            );
             
            //return our $Data array to use on the page
            return $this->Customise($Data);
        }
        else
        {
            //Staff member not found
            return $this->httpError(404, 'Deja nieko panasaus neradau. :/ Keista.');
        }
    }
}

