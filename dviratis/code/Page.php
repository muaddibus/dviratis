<?php
class Page extends SiteTree {

        private static $db = array(
        );

        private static $has_one = array(
        );
        public function PatvirtintiObjektai() {
                return Objektas::get("Objektas")->filter(array("Patvirtinta"=>true));
        }
        public function LinkPhoto() {
    		return "android-icon-192x192.png";
        }

}
class Page_Controller extends ContentController {
        private static $allowed_actions = array (
        );
        public function init() {
                parent::init();
                $themeFolder = $this->ThemeDir();
                Requirements::set_combined_files_folder($themeFolder . '/combinedfiles');
                $CSSFiles = array(
                    $themeFolder . '/css/bootstrap.min.css',
                    $themeFolder . '/css/photoswipe-skin.css',
                    $themeFolder . '/css/photoswipe.css',
                    $themeFolder . '/css/print.css',
                    $themeFolder . '/css/form.css',
                    $themeFolder . '/css/typography.css',
                    $themeFolder . '/css/jquery.growl.css',
                    $themeFolder . '/css/layout.css'
                );
                Requirements::combine_files("combinedCSS.min.css", $CSSFiles);

                $JAVAFiles = array(
                    $themeFolder . '/javascript/jquery.min.js',
                    $themeFolder . '/javascript/bootstrap.min.js',
                    $themeFolder . '/javascript/jquery.growl.js',
                    $themeFolder . '/javascript/photoswipe-ui-default.min.js',
                    $themeFolder . '/javascript/photoswipe.min.js',
                    $themeFolder . '/javascript/markerclusterer_compiled.js',
                    $themeFolder . '/javascript/main.js'
                );
                Requirements::combine_files("combinedJAVA.min.js", $JAVAFiles);
                Requirements::process_combined_files();
        }
}


