<?php
class Nuotrauka extends Image {
        private static $has_one = array(
                'Objektas' => 'Objektas',
                'SortOrder' => 'Int',
        );
        private static $default_sort='SortOrder';
        public function generateTHMB(GD $gd) {
                return $gd->croppedResize(200,70);
        }
        
        public function RatioCrop($width, $height) {
		return $this->owner->isSize($width, $height)
		? $this->owner
		: $this->owner->getFormattedImage('RatioCrop', $width, $height);
	}
	public function generateRatioCrop(Image_Backend $backend, $width, $height) {
		$this->owner->generateFormattedImage('SetRatioSize', $width, $height);
		return $backend->croppedResize($width, $height);
	}

}

