<?php
class Objektas extends DataObject {
        private static $db = array(
                'Title' => 'Varchar',
                'Description' => 'HTMLText',
                'Zoom' => 'Float',
                'Patvirtinta' => 'Boolean',
                'Sutvarkyta' => 'Boolean',
                'SutvarkytaData' => 'Date',
                'Latitude' => 'Float(10,6)',
		'Longitude' => 'Float(10,6)',
                // Objektas google maps
                'SortOrder'=>'Int',
        );
        private static $has_many = array(
                'Nuotraukos' => 'Nuotrauka',
        );
        private static $has_one = array(
        	'Objektai' => 'Objektai',
        );
	static $many_many = array(
		'Tagai' => 'Tagas'
	);
        private static $default_sort='SortOrder';
        private static $singular_name = 'Objektas';
        private static $plural_name = 'Objektai';

        private static $field_labels = array(
                'Title' => 'Pavadinimas',
                'Description' => 'Aprasymas',
                'Patvirtinta' => 'Patvirtinta',
                'Sutvarkyta' => 'Sutvarkyta',
                'SutvarkytaData' => 'Kada Sutvarkyta',
        );
        private static $searchable_fields = array(
                'Title',
                'Aprasymas',
        );
        private static $summary_fields = array(
                'Title',
                'Description',
                'Svarbumas',
                'Sutvarkyta',
                'Patvirtinta',
        );
        private static $defaults = array(
    	    'Patvirtinta' => false,
    	    'Sutvarkyta' => false,
    	);
        public function getCMSFields() {
                $fields = parent::getCMSFields();
                $fields->removeFieldFromTab("Root","SortOrder");
                $fields->removeFieldFromTab("Root","Longitude");
                $fields->removeFieldFromTab("Root","Latitude");
                $fields->removeFieldFromTab("Root","Zoom");

		$dateField = new DateField('SutvarkytaData','Kada Sutvarkyta');
		$dateField->setConfig('showcalendar', true);
		$dateField->setConfig('dateformat','dd.MM.yyyy');
		$fields->addFieldToTab('Root.Main',$dateField);
		
                $fields->addFieldToTab('Root.Nuotraukos',$galerija = new SortableUploadField('Nuotraukos'));
                $galerija->getValidator()->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
                $galerija->setConfig('CanAttachExisting',false);
                $galerija->setFolderName('Objektai/Objektas-'.$this->ID."/");
		$fields->addFieldToTab('Root.Main',new GoogleMapField($this,'Vieta zemelapyje'));
                return $fields;
        }
        
        public function NetvarkymoTrukme() {
		$datetime1 = strtotime(date('Y-m-d H:i:s'));
		$datetime2 = strtotime($this->Created);

		$secs = $datetime1 - $datetime2;
		$days = round($secs / 86400);
		return $days." d.";
        }
        public function Link() {
		if($Objektai = $this->Objektai())
	        {
	            $Action = 'show/' . $this->ID;
	            return $Objektai->Link($Action);    
	        } 
        }
        public function Sukurta() {
		return $this->Created->Format("Y-m-d");
        }
        public function canView($member = null) {
    	    return true;
	}
}

